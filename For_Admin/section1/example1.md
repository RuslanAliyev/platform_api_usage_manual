#View all Users and their Info#

In: `GET http://113.196.245.218:3000/users?t=TOKEN...`

Out:

 - `{code:'1', msg:'Only administrator can do this'}`
 
 - `{"code":"0","msg":[{"_id":"MONGODB_ID","un":"USER_NAME","email":"EMAIL","pw":"CRYPTED_PASSWORD","permission":"PERMISSION"} , {"_id":"__","un":"__","email":"__","pw":"__","permission":"__"} , ...]}`
 
 - `{code:'-1', msg:'Login before you do anything'}`

 
#Create an User Account directly#

In: `POST http://113.196.245.218:3000/userByAdmin?t=TOKEN...` AND `{"un":"USER_NAME", "email":"EMAIL", "pw":"PASSWORD", "permission":"PERMISSION"}`

Out:

 - `{code:'1', msg:'Only administrator can do this'}`
 
 - `{code:'0', msg:'New user succesfully created'}`
 
 - `{code:'-1', msg:'Login before you do anything'}`


#Edit an User Account's Info#

In: `PUT http://113.196.245.218:3000/user?t=TOKEN...` AND `{"_id":"MONGODB_ID", "un":"NEW_USER_NAME", "email":"NEW_EMAIL", "pw":"NEW_PASSWORD", "permission":"NEW_PERMISSION"}`

Out:

 - `{code:'1', msg:'Only administrator can do this'}`
 
 - `{code:'0', msg:'User succesfully edited'}`
 
 - `{code:'-1', msg:'Login before you do anything'}`


#Delete an User Account#

In: `DELETE http://113.196.245.218:3000/user?t=TOKEN...` AND `{"un":"USER_NAME"}`

Out:

 - `{code:'1', msg:'Only administrator can do this'}`
 
 - `{code:'0', msg:'User succesfully deleted'}`
 
 - `{code:'-1', msg:'Login before you do anything'}`