# Summary

User's Documentation for: Collaborative Platform API (2015-03-31)

* [PlaySDK API's Documentation](section1/README.md)
    * [User Accounts CRUD](section1/example1.md)
    * [Shared Files CRUD](section1/example2.md)
	* [Issues List CRUD](section1/example3.md)	
    * [Uploading Projects (Provisional)](section1/example4.md)	
    * [Uploading Projects (Future)](section1/example5.md)
	* [Gradle](section1/example6.md)