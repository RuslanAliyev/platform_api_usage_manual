#Project Uploader (Provisional) overview#

This platform allows you to upload a zipped project folder, have it processed, pushed to online repository, built, then get you notified, all in one automatic and continous go.

The type of projects that you can upload are either Android projets made in AndroidStudio, or Gitbook projects.

No need to include gitignore files into your projects, the platform will do this for you.

If you are uploading Android projects, make sure to remove all build folders, because the build folder contains large quantities of unnessecary files.

![](https://lh4.googleusercontent.com/GM3ZZcFJ2dhrlFxYKpjL_0i3UlFNk8CdvYe0efVAqeNKneKh_xpdcmLqWTA8fdQGUYOPog=w1896-h459)

If you are uploading Git-Book projects, make sure to remove the `_book` folder before uploading them.

This platform can only process Android projects made in AndroidStudio, because the automatic process relies on gradle. Refer to the next section for guidelines to how to write gradle files, in order for the platform to build your Android projects properly.

You will need to implement this method for all of this:

`POST http://113.196.245.218:3000/project/'{TYPE}'?zip=true&t=TOKEN...`

#The Project Uploading Part#

`POST http://113.196.245.218:3000/project/'{TYPE}'?zip=true&t=TOKEN...` AND a form-data POST object that contain the project files. See illustrations below.

Type is either Android or Gitbook.

Project name is the name of the root folder of your project. This name will also be the name of your projects online repository and the name that your project will be recorded under on our servers. If you want to rename your project, then just rename the root folder of your project. See below for illustration

![](https://lh5.googleusercontent.com/dhv5mY_m7Z3PF_bXTbJEatJ_55dLS_mcpAUZM3QA5K4gHcMnqqqIIyAQEEs4ObOQuVmGcQ=w1896-h459 "The root project folder's name will also be your project's name")

`token` is the token that you got when you logged in.

You can implement the front-end like this:

```html

	<form method='post' action='http://113.196.245.218:3000/project/PROJECT_TYPE?t=TOKEN...&new=BOOLEAN-NEW_PROJECT_OR_NOT&root=ROOT_PROJECT_FOLDER_NAME' encType='multipart/form-data'>
	  <input type='file' name='file' />
	  <input type='submit' />
	</form>

```

`new` : You need to include a checkbox or something similar on your front-end, to allow user to state whether or not the project has been uploaded onto this platform before. `true` mean that this project is being uploaded for the first time.

Result:

 - Redirection to: `YOUR_DOMAIN_NAME/uploadView/PROJECT_TYPE?t=TOKEN...&msg=SOME_SUCCESS_OR_ERROR_MESSAGE` . 
 