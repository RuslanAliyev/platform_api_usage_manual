#View an Issue#

In: `GET http://113.196.245.218:3000/issue/'{ISSUE_NAME}'?t=TOKEN...`

Out:

 - `{"code":"0","msg":{"_id":"MONGODB_ID","name":"SOME_NAME","content":"SOME_CONTENT"}}`

 - `{code:'-1', msg:'Login before doing anything'}`
 

#View all Issues#

In: `GET http://113.196.245.218:3000/issues?t=TOKEN...`

Out:

 - `{"code":"0","msg":[{"_id":"MONGODB_ID","name":"SOME_NAME","content":"SOME_CONTENT"},{"_id":"__","name":"__","content":"__"}, ...]}`
 
 - `{code:'-1', msg:'Login before you do anything'}`
 

#Create an Issue#

In: `POST http://113.196.245.218:3000/issue?t=TOKEN...` AND `{"name":"__","content":"__"}` . See Illustration Below

![](https://lh5.googleusercontent.com/qNQlSl_3bXDiqOnwoHiK8CnUXWIJkXMLhMWj6lgLlptQKU8g-Gnk3Buw0vZTS3yCFiWmbQ=w1896-h815 "Posting an issue as JSON")

Out:

 - `{code:'0', msg:'Issue entered successfully'}`
 
 - `{code:'-1', msg:'Login before you do anything'}`
 

#Edit an Issue#

In: `PUT http://113.196.245.218:3000/issue?t=TOKEN...` AND `{"_id":"THE_MONGO_DB_ID","name":"NEW_NAME","content":"NEW_CONTENT"}` . See Illustration Below

![](https://lh5.googleusercontent.com/jWaNEWuZ47b0pyj2XYQ6LkDXOpeOKXZwpk8It00I7-F1agfzE4PTiyZwx6KfhhyBGVmXJg=w629-h459 "Updating an issue")

> Note: Be sure to use the same MongoDB ID when updating issues. Otherwise the issue that you wanted to be update wont update properly. The MongoDB ID can be obtained using the GET /issue/ISSUE_NAME method.

Out:

 - `{code:'0', msg:'Issue updated successfully'}`
 
 - `{code:'-1', msg:'Login before you do anything'}`
 

#Delete an Issue#

In: `DELETE http://113.196.245.218:3000/issue?t=TOKEN...` AND `{"name":"ISSUE_NAME"}`

Out:

 - `{code:'0', msg:'Issue deleted successfully'}`
 
 - `{code:'-1', msg:'Login before you do anything'}`