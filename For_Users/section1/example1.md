#Sign Up#

In: `POST http://113.196.245.218:3000/user` AND `{"un":"___", "email":"___", "pw1":"___", "pw2":"___"}`

Out: 
 
 - `{code:'2', msg:'Email already exists'}`
 
 - `{code:'1', msg:'Username already exists'}`

 - `{code:'0', msg:'Check your email'}` . This means that an email with the verification url has been sent to you. The verification email's format: `YOUR_DOMAIN_NAME/verification/'{Code}'` . 
 
The emailed link will redirect you to back to your side. Hence you'll have to handle the route: `YOUR_DOMAIN_NAME/verification/'{Code}'` . 

After you retrieve the `code` you have to send the code to `GET http://113.196.245.218:3000/userVerification/'{Code}'` . (See next section 'Verifying Signup')
  
#Verifying Signup#

In: `GET http://113.196.245.218:3000/userVerification/'{Code}'`

Out:

 - `{code:'0', msg:'Email verification succeeded. You can log in now'}` . Note: The default permission that a user gets is 'Group_1'
 
 - `{code:'1', msg:'Email verification failed. Try again'}`
 

#Login#

> This mimics the 2 step OAuth2 login process. Step 1/2 retrieves key via redirection-URL. Step 2/2 gets token with key.

##Authorize##

In: `POST http://113.196.245.218:3000/key` AND `{"un":"___", "pw":"___"}`

Out: 

 - If not yet registered: Automatically redirected to an URL of this format: `YOUR_DOMAIN_NAME?msg=Wrong password` . You can extract the 'msg' variable out of the URL for your own use.
 
 - If wrong password: Automatically redirected to an URL of this format: `YOUR_DOMAIN_NAME?msg=Signup first` . You can extract the 'msg' variable out of the URL for your own use.
 
 - If success: Automatically redirected to an URL of this format: `YOUR_DOMAIN_NAME/callback?k=KEY...` . From this URL you can get the key. 

##Receive Token##

In: `POST http://113.196.245.218:3000/token` AND `{"key": YOUR_KEY}`

Out:

 - `{code:'1', msg:'Login failed. Try again'}`
 
 - `{code:'0', msg: YOUR_TOKEN}`

 
#User get user's own info#

In: `GET http://113.196.245.218:3000/user?t=TOKEN...`

Out:

 - `{code:'1', msg:'No such user found in our system'}`
 
 - `{"code":"0", "msg":{"_id":"MONGODB_ID", "un":"YOUR_USER_NAME", "email":"YOUR_EMAIL", "pw":"CRYPTED_PASSWORD", "permission":"YOUR_PERMISSION"}}`
 
 - `{code:'-1', msg:'Login before doing anything'}`
 
 
#Logout#

In: `DELETE http://113.196.245.218:3000/token?t=TOKEN...`

Out:

 - `{code:'0', msg:'Logout done'}`
 
 - `{code:'-1', msg:'You are not logged in'}`