## For the Application's Gradle File ##

```groovy

apply plugin: 'com.android.application'

android {
    compileSdkVersion 21
    buildToolsVersion "21.1.2"

    defaultConfig {
        applicationId "com.example.fg000030.myapplication"
        minSdkVersion 9
        targetSdkVersion 21

        //*********************************************************
        def manifestFile = file('./src/main/AndroidManifest.xml')
        def ns = new groovy.xml.Namespace("http://schemas.android.com/apk/res/android", "android")
        def xml = new XmlParser().parse(manifestFile)

        def versionName = xml.attributes()[ns.versionName].toString()
        def versionCode = xml.attributes()[ns.versionCode].toString()
        def ver = versionName + "_" + versionCode

        def region = 'tw' //tw for Taiwan or hk for HongKong
        def reseller = 'mm' //eg: mm
        def platform = '2' //The platform number
        def gm = 'GM' //GM Model

        android.applicationVariants.all { variant ->
            variant.outputs.each { output ->
                def outputFile = output.outputFile
                if (outputFile.name.contains('debug.apk')) {
                    output.outputFile = new File(outputFile.parent, region + "_" + reseller + "_" + ver + "_beta_" + platform + "_" + gm +".apk")
                }
                if (outputFile.name.contains('release.apk')) {
                    output.outputFile = new File(outputFile.parent, region + "_" + reseller + "_" + ver + "_release_" + platform + "_" + gm +".apk")
                }
            }
        }
    }

    signingConfigs {
        release {
            storeFile file("C:/Users/FG000030/android.keystore")
            storePassword "gggggg"
            keyAlias "android.keystore"
            keyPassword "gggggg"
        }
    }
    buildTypes {
        release {
            minifyEnabled false
            signingConfig signingConfigs.release
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
        //*********************************************************
    }
}

dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    compile 'com.android.support:appcompat-v7:21.0.3'
}

```


### These lines reads the version code and number from the Manifest file ###

```

def manifestFile = file('./src/main/AndroidManifest.xml')

def ns = new groovy.xml.Namespace("http://schemas.android.com/apk/res/android", "android")

def xml = new XmlParser().parse(manifestFile)


def versionName = xml.attributes()[ns.versionName].toString()

def versionCode = xml.attributes()[ns.versionCode].toString()

def ver = versionName + "_" + versionCode

```

#### Assuming that your Manifest file looks something like this ####

```

<manifest xmlns:android="http://schemas.android.com/apk/res/android"

package="com.forgame.playsdk"

android:versionCode="4"

android:versionName="1.7.0" >
	
```


### Include the following chunk of code, provide the region, reseller name, platform code and GM Model name ###

```

def region = 'tw' //tw for Taiwan or hk for HongKong

def reseller = 'mm' //eg: mm

def platform = '2' //The platform number

def gm = 'GM' //GM Model
		
```


### Include the following chunk of code exactly as it is ###

```

android.applicationVariants.all { variant ->

	variant.outputs.each { output ->
	
		def outputFile = output.outputFile
		
		if (outputFile.name.contains('debug.apk')) {
		
			output.outputFile = new File(outputFile.parent, region + "_" + reseller + "_" + ver + "_beta_" + platform + "_" + gm +".apk")
			
		}
		
		if (outputFile.name.contains('release.apk')) {
		
			output.outputFile = new File(outputFile.parent, region + "_" + reseller + "_" + ver + "_release_" + platform + "_" + gm +".apk")
			
		}
		
	}
	
}
		
```


### Also include the following exactly as it is ###

```

signingConfigs {

	release {
	
		storeFile file("C:/Users/FG000030/android.keystore")
		
		storePassword "gggggg"
		
		keyAlias "android.keystore"
		
		keyPassword "gggggg"
		
	}
	
}

buildTypes {

	release {
	
		minifyEnabled false
		
		signingConfig signingConfigs.release
		
		proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
		
	}

}
	
```


### DELETE all the build folders from your project before uploading them to the platform. DO NOT include them ###

![](https://lh4.googleusercontent.com/GM3ZZcFJ2dhrlFxYKpjL_0i3UlFNk8CdvYe0efVAqeNKneKh_xpdcmLqWTA8fdQGUYOPog=w1896-h459)




## For the Library's Gradle File ##

```groovy

apply plugin: 'com.android.library'

android {
    compileSdkVersion 21
    buildToolsVersion "21.1.2"

    defaultConfig {
        minSdkVersion 9
        targetSdkVersion 19

        //*********************************************************
        def manifestFile = file('./src/main/AndroidManifest.xml')
        def ns = new groovy.xml.Namespace("http://schemas.android.com/apk/res/android", "android")
        def xml = new XmlParser().parse(manifestFile)

        def versionName = xml.attributes()[ns.versionName].toString()
        def versionCode = xml.attributes()[ns.versionCode].toString()
        def ver = versionName + "_" + versionCode

        android.libraryVariants.all { variant ->
            variant.outputs.each { output ->
                def outputFile = output.outputFile
                if (outputFile.name.contains('debug.aar')) {
                    output.outputFile = new File(outputFile.parent, outputFile.name.replace(".aar", ver + ".aar"))
                }
                if (outputFile.name.contains('release.aar')) {
                    output.outputFile = new File(outputFile.parent, outputFile.name.replace(".aar", ver + ".aar"))
                }
            }
        }
        //*********************************************************
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    compile 'com.android.support:appcompat-v7:21.0.3'
}

```
