#Project Uploader (Future) overview#

This platform allows you to upload a project folder, have it processed, pushed to online repository, built, then get you notified, all in one automatic and continous go.

You have the option to upload a folder or a zip folder. Zip folder may become useful if your clients are not using Chrome, for example.

The type of projects that you can upload are either Android projets made in AndroidStudio, or Gitbook projects.

No need to include gitignore files into your projects, the platform will do this for you.

If you are uploading Android projects, make sure to remove all build folders, because the build folder contains large quantities of unnessecary files.

![](https://lh4.googleusercontent.com/GM3ZZcFJ2dhrlFxYKpjL_0i3UlFNk8CdvYe0efVAqeNKneKh_xpdcmLqWTA8fdQGUYOPog=w1896-h459)

If you are uploading Git-Book projects, make sure to remove the `_book` folder before uploading them.

This platform can only process Android projects made in AndroidStudio, because the automatic process relies on gradle. Refer to the next section for guidelines to how to write gradle files, in order for the platform to build your Android projects properly.

You will need to implement this method for all of this:

`POST http://113.196.245.218:3000/project/'{TYPE}'?zip=false&t=TOKEN...`

#The Project Uploading Part#

`POST http://113.196.245.218:3000/project/'{TYPE}'?zip=false&t=TOKEN...` AND a form-data POST object that contain the project files. See illustrations below.

Type is either Android or Gitbook.

Project name is the name of the root folder of your project. This name will also be the name of your projects online repository and the name that your project will be recorded under on our servers. If you want to rename your project, then just rename the root folder of your project. See below for illustration

![](https://lh5.googleusercontent.com/dhv5mY_m7Z3PF_bXTbJEatJ_55dLS_mcpAUZM3QA5K4gHcMnqqqIIyAQEEs4ObOQuVmGcQ=w1896-h459 "The root project folder's name will also be your project's name")

`zip` is either set to `true` or `false` . If your are uploading a zip folder, then it will be `true`, and vice versa.

`token` is the token that you got when you logged in.

The form-data POST object should be structured like this, if you are uploading a folder:

![](https://lh3.googleusercontent.com/IYf0SFCXyA5G_hW3Ukt4uede58qB7_xKxXn11Jz5SC8WPYcW1tufvD7BgjneA2ftBvc0hw=w1896-h859)

 - `isNew` can either be set to `true` or `false` . `true` means this is the first time that you have uploaded this project onto this platform. `false` means that you already uploaded this project onto this platform before.
 
 - `root` is the root folder of your project. 
 
 - Then you list all the files in the project folder in path-and-file pairs.
 
You can implement this in AJaX like this:

```html

    <div>
		<form>
			<input type="file" id="files" name="files[]" multiple="" webkitdirectory="" />
			<button type='button' onclick="folderFormProcess()"></button>
		</form>
	</div>

```

```javascript

	function folderFormProcess(){
		var isNew = document.getElementById('isNew').checked;
		uploadFiles(document.getElementById('files').files, isNew);
	}	

	function uploadFiles(files, isNew){
		xhr = new XMLHttpRequest();
		data = new FormData();
		data.append('isNew', isNew);
		data.append('root', files[0].webkitRelativePath);
		
		for (var i in files){
			data.append('path', files[i].webkitRelativePath);
			data.append(i, files[i]);
		};

		xhr.onreadystatechange=function(){
			if (xhr.readyState==4 && xhr.status==200){
				console.dir(xhr.responseText);
			}
		}

		xhr.open('POST', 'http://113.196.245.218:3000/project/android?zip=false&t=6732blahg6t', true);
		xhr.send(this.data);    
	}
	
```

The form-data POST object should be structured like this, if you are uploading a zip folder:

![](https://lh4.googleusercontent.com/scoPqEI3wPKDD3_NkYIpN8yL8dRuRQTl3ApHVE0Pj8dbruHtmNlTZXP8OOTrSfdXXz_D4g=w1896-h859)

You can implement this in AJaX like this:

```html

    <div>
		<form>
			<input type="file" id="zip" />
			<button type='button' onclick="zipFormProcess()"></button>
		</form>
	</div>

```

```javascript

	function zipFormProcess(){
        var isNew = document.getElementById('isNew').checked;
        uploadFile(document.getElementById('zip').files[0], isNew);
	}	

	function uploadFile(file, isNew){
		xhr = new XMLHttpRequest();
		data = new FormData();
		data.append('isNew', isNew);
		data.append('root', file.name);
		data.append('zip', file); 

		xhr.onreadystatechange=function(){
			if (xhr.readyState==4 && xhr.status==200){
				console.dir(xhr.responseText);
			}
		}

		xhr.open('POST', 'http://113.196.245.218:3000/project/android?zip=true&t=87bblah5da4', true);
		xhr.send(this.data);
	}
	
```

The results can be:

 - `{code:'4', msg:'__'}` . Somethig went wrong while transferring your project files to the remote repository. Try again later.
 
 - `{code:'3', msg:'__'}` . Somethig went wrong on the server. Try again later.

 - `{code:'2', msg:'__'}` . Somethig went wrong in the remote repository. Try again later.
 
 - `{code:'1', msg:'Another project exists with the same name. Change your project folders name or uncheck isNew'}` 
 
 - `{code:'1', msg:'Dont include the build folders! Upload again without the build folders'}`
 
 - `{code:'1', msg:'Upload an Android project! Upload again properly'}`
 
 - `{code:'1', msg:'Upload an Gitbook project! Upload again properly'}`
 
 - `{code:'0', msg:'project succesfully uploaded and processed'}`
 
 - `{code:'-1', msg:'Login before doing anything'}`