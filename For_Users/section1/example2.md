#View all Files in a Folder#

In: `GET http://113.196.245.218:3000/files?t=TOKEN...` , OR: `GET http://113.196.245.218:3000/files?t=TOKEN...&dir=SOME/PATH`

Out:

 - `{"code":"0","msg":["SOME_FOLDER_NAME", "SOME_FILE_NAME", ... ],"dir":"CURRENT/FOLDER","home":"USER/HOME/FOLDER"}`
 
 - `{code:'-1', msg:'Login before doing anything'}`
 

#Navigate into a folder, get a file or view a gitbook#

In: `GET http://113.196.245.218:3000/file/'{INNER_FOLDERS_NAME}'?dir=OUTER/FOLDERS/NAMES&t=TOKEN...` , OR: `GET http://113.196.245.218:3000/file/'{FILE_NAME_WITH_EXT}'?dir=PATH/NAME&t=TOKEN...`

Example:

```

 - Folder_A
 -- Folder_B
 --- File_C.ext
 
 To get Folder B, type `GET http://113.196.245.218:3000/file/Folder_B?dir=Folder_A&t=TOKEN...`
 
 To get File_C.ext, type `GET http://113.196.245.218:3000/file/File_C.ext?dir=Folder_A/Folder_B&t=TOKEN...`
 
```

Out:

 - `{code:'1', msg: 'SOME/PATH/index.html'}` , If you are to read a gitbook.
 
 - `{"code":"0","msg":"SOME/PATH/FOLDER_NAME"}` , if you are trying to go into a folder. OR: The actual file bytes in return, if you are trying to get a file. See illustration below.

 - `{code:'-1', msg:'Login before doing anything'}`
 
![](https://lh5.googleusercontent.com/FUgIu99PkHoCY5JyK4qGZ1dOSZfb7wpYFXs1DCrC7OVN1kYil1Qb1D_WNGhnDzjdrMrrbA=w1896-h459 "Getting a file's bytes in return")


#Upload Files#
 
In: `POST http://113.196.245.218:3000/file?dir=SOME/PATH&t=TOKEN...` AND the file in form-data format. See illustration below.

![](https://lh3.googleusercontent.com/YC6twOTPjYSYvqRqaEbVBxnFARb-1eEZfhULqc4ZdGfZSGGp0txcZH0cWfk_lVTv7F9hYw=w1896-h815 "Uploading File or Files")

Out:

 - If successful, you'll be redirected to : `http://YOUR_DOMAIN_NAME/fileView?msg=All uploaded succesfully&dir=CURRENT/FOLDER&t=TOKEN...`
 
 - If token expired, you'll be redirected to : `http://YOUR_DOMAIN_NAME/?msg=Login before doing anything`


#Rename File#

In: `PUT http://113.196.245.218:3000/file/'{OLD_FILE_NAME}'/'{NEW_FILE_NAME}'?dir=SOME/PATH&t=TOKEN...`

Out:

 - `{code:'2', msg:'Only administrator can go in here'}`
 
 - `{code:'1', msg:'Only administrator can rename files here'}`
 
 - `{code:'0', msg: 'SOME/PATH'}`

 - `{code:'-1', msg:'Login before doing anything'}`
 
 
#Delete File#

In: `DELETE http://113.196.245.218:3000/file/'{FILE_NAME}'?dir=SOME/PATH&t=TOKEN...`

Out:

 - `{code:'2', msg:'Only administrator can go in here'}`
 
 - `{code:'1', msg:'Only administrator can delete files here'}`
 
 - `{code:'0', msg: 'SOME/PATH'}`

 - `{code:'-1', msg:'Login before doing anything'}`